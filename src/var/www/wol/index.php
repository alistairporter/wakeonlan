<?php /*
Remote Wake/Sleep-On-LAN Server
https://github.com/JoeWalters/Remote-Wake-Sleep-On-LAN-Server
Forked from: https://github.com/sciguy14/Remote-Wake-Sleep-On-LAN-Server
Original Author: Jeremy E. Blum (http://www.jeremyblum.com)
Security Edits By: Felix Ryan (https://www.felixrr.pro)
License: GPL v3 (http://www.gnu.org/licenses/gpl.html)
*/ 

//You should not need to edit this file. Adjust Parameters in the config file:
require_once('config.php');

//set headers that harden the HTTPS session
if ($USE_HTTPS)
{
   header("Strict-Transport-Security: max-age=7776000"); //HSTS headers set for 90 days
}

// Enable flushing
ini_set('implicit_flush', true);
ob_implicit_flush(true);
ob_end_flush();

//Set the correct protocol
if ($USE_HTTPS && !$_SERVER['HTTPS'])
{
   header("Location: https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
   exit;
}

//Set default computer (this is business logic so should be done last)
if (empty($_GET))
{
   header('Location: '. ($USE_HTTPS ? "https://" : "http://") . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?computer=0");
   exit;
}
else
   $_GET['computer'] = preg_replace("/[^0-9,.]/", "", $_GET['computer']);

?>

<!DOCTYPE html>
<html lang="en" >
  <head>
    <title>Remote Wake/Sleep-On-LAN</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A utility for remotely waking/sleeping a Windows computer via a Raspberry Pi">
    <meta name="author" content="Jeremy Blum">

    <!-- Le styles -->
    <link href="res/css/main.css" rel="stylesheet">
    <link href="res/css/navbar.css" rel="stylesheet">

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="res/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="res/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="res/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="res/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="res/ico/favicon.png">
    
    <!-- Dark Mode!!!! -->
    <script>
		document.addEventListener('DOMContentLoaded', (event) => {((localStorage.getItem('mode') || 'dark') === 'dark') ? document.querySelector('body').classList.add('dark') : document.querySelector('body').classList.remove('dark')})
	</script>
  </head>

  <body class="dark">
    <<navbar>
		<ul>
			<li><a href="#" title="Toggle Night Mode" onclick="localStorage.setItem('mode', (localStorage.getItem('mode') || 'dark') === 'dark' ? 'light' : 'dark'); localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('dark') : document.querySelector('body').classList.remove('dark')"><svg style="width:1em; height:1em" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="moon" class="svg-inline--fa fa-moon fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M283.211 512c78.962 0 151.079-35.925 198.857-94.792 7.068-8.708-.639-21.43-11.562-19.35-124.203 23.654-238.262-71.576-238.262-196.954 0-72.222 38.662-138.635 101.498-174.394 9.686-5.512 7.25-20.197-3.756-22.23A258.156 258.156 0 0 0 283.211 0c-141.309 0-256 114.511-256 256 0 141.309 114.511 256 256 256z"></path></svg></a><div class="navbar-underline"></div></li>
			<li><a href="https://aporter.xyz/" title="Home">Home</a><div class="navbar-underline"></div></li>
		</ul>
	</navbar>
    <main>
    	<form class="form-signin" method="post">
        	<h3 class="page-title">
			<?php
				//print_r($_POST); //Useful for POST Debugging
				$approved_wake = false;
				$approved_sleep = false;
				//if ( isset($_POST['password']) )
		                //{
                                        if ($USE_PASS == "false")
                                        {
                                                if ($_POST['submitbutton'] == "Wake Up!")
                                                {
                                                        $approved_wake = true;
                                                }
                                                elseif ($_POST['submitbutton'] == "Sleep!")
                                                {
                                                        $approved_sleep = true;
                                                }
                                        }
                			$hash = hash("sha256", $_POST['password']);
			                //print_r($hash)
					if ($hash == $APPROVED_HASH)
			                {
						if ($_POST['submitbutton'] == "Wake Up!")
						{
							$approved_wake = true;
						}
						elseif ($_POST['submitbutton'] == "Sleep!")
						{
							$approved_sleep = true;
						}
					}
				//}

				$selectedComputer = $_GET['computer'];

			 	echo "Remote Wake/Sleep-On-LAN</h3>";
				if ($approved_wake) {
					echo "Waking Up!";
					echo '<div class="page-description"> </div>';
				} elseif ($approved_sleep) {
					echo "Going to Sleep!";
					echo '<div class="page-description"> </div>';
				} else {?>
				    <h5>
                    <select class="selector" name="computer" onchange="if (this.value) window.location.href='?computer=' + this.value">
                    <?php
                        for ($i = 0; $i < count($COMPUTER_NAME); $i++)
                        {
                            echo "<option value='" . $i;
                            if( $selectedComputer == $i)
							{
								echo "' selected>";
							}
                            else
							{
								echo "'>";
							}
							echo $COMPUTER_NAME[$i] . "</option>";
                
                        }
                    ?>
                    </select>

				<?php } ?>
            <?php



                $show_form = true;

                if ($approved_wake)
                {
                	echo "<p>Approved. Sending WOL Command...</p>";
					exec ('wakeonlan ' . $COMPUTER_MAC[$selectedComputer]);
					echo "<p>Command Sent. Waiting for " . $COMPUTER_NAME[$selectedComputer] . " to wake up...</p><p>";
					$count = 1;
					$down = true;
					while ($count <= $MAX_PINGS && $down == true)
					{
						echo "Ping " . $count . "...";
						$pinginfo = exec("ping -c 1 " . $COMPUTER_LOCAL_IP[$selectedComputer]);
						$count++;
						if ($pinginfo != "")
						{
							$down = false;
							echo "<span style='color:#00CC00;'><b>It's Alive!</b></span><br />";
							echo "<p><a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a></p>";
							$show_form = false;
						}
						else
						{
							echo "<span style='color:#CC0000;'><b>Still Down.</b></span><br />";
						}
						sleep($SLEEP_TIME);
					}
					echo "</p>";
					if ($down == true)
					{
						echo "<p style='color:#CC0000;'><b>FAILED!</b> " . $COMPUTER_NAME[$selectedComputer] . " doesn't seem to be waking up... Try again?</p><p>(Or <a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a>.)</p>";
					}
				}
				elseif ($approved_sleep)
				{
					echo "<p>Approved. Sending Sleep Command...</p>";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://" . $COMPUTER_LOCAL_IP[$selectedComputer] . ":" . $COMPUTER_SLEEP_CMD_PORT . "/" .  $COMPUTER_SLEEP_CMD);
					curl_setopt($ch, CURLOPT_TIMEOUT, 5);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					
					if (curl_exec($ch) === false)
					{
						echo "<p><span style='color:#CC0000;'><b>Command Failed:</b></span> " . curl_error($ch) . "</p>";
                                                echo "<p><a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a></p>";
                                                $show_form = false;
					}
					else
					{
						echo "<p><span style='color:#00CC00;'><b>Command Succeeded!</b></span> Waiting for " . $COMPUTER_NAME[$selectedComputer] . " to go to sleep...</p><p>";
						$count = 1;
						$down = false;
						while ($count <= $MAX_PINGS && $down == false)
						{
							echo "Ping " . $count . "...";
							$pinginfo = exec("ping -c 1 " . $COMPUTER_LOCAL_IP[$selectedComputer]);
							$count++;
							if ($pinginfo == "")
							{
								$down = true;
								echo "<span style='color:#00CC00;'><b>It's Asleep!</b></span><br />";
								echo "<p><a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a></p>";
							}
							else
							{
								echo "<span style='color:#CC0000;'><b>Still Awake.</b></span><br />";
                                                                echo "<p><a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a></p>";
							}
							sleep($SLEEP_TIME);
						}
						echo "</p>";
						if ($down == false)
						{
							echo "<p style='color:#CC0000;'><b>FAILED!</b> " . $COMPUTER_NAME[$selectedComputer] . " doesn't seem to be falling asleep... Try again?</p><p>(Or <a class='button' href='?computer=" . $selectedComputer . "'>Return to the Wake/Sleep Control Home</a>.)</p>";
						}
					}
					curl_close($ch);
				}
				elseif (isset($_POST['submitbutton']))
				{
					echo "<p style='color:#CC0000;'><b>Invalid Passphrase. Request Denied.</b></p>";
				}		
                
                if ($show_form)
                {
			if ($USE_PASS != "false") {
            
			echo '<input type="password" autocomplete=off class="input-block-level" placeholder="Enter Passphrase" name="password">';
			}
			echo '<input class="button" type="submit" name="submitbutton" value="Wake Up!"/>';
			if ($ENABLE_SLEEP == "true") {

			echo '<input class="button" type="submit" name="submitbutton" value="Sleep!"/>';
			}
				}	
